// #5 http module
let http = require("http");

// #6 port variable
const port = 3000;

// #7 create server that listens to defined port

const server = http.createServer((request, response) => {

	// #9 login page condition
	if(request.url == "/login")
	{
		response.writeHead(200,{"Content-Type": "text/plain"});
		response.end("Welcome to the login page.");
	}

	// #11 error massage for all other routes
	else
	{
		response.writeHead(200,{"Content-Type": "text/plain"});
		response.end("I'm sorry the page you are looking for cannot be found.");
	}


});

server.listen(port);

// #8 console logging server run message
console.log(`Server is successfully running. listening to port: ${port}`);